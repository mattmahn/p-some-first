module.exports = {
	plugins: [
		"@semantic-release/commit-analyzer",
		[
			"@semantic-release/release-notes-generator",
			{
				preset: "conventionalcommits",
			},
		],
		"@semantic-release/changelog",
		"@semantic-release/npm",
		[
			"@semantic-release/git",
			{
				assets: ["CHANGELOG.md", "package*.json"],
			},
		],
		"@semantic-release/gitlab",
	],
};
