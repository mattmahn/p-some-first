export interface CancelablePromise<T> extends Promise<T> {
	cancel?(reason?: any): any;
}

/**
 * Reolves to the value of the first resolving task, or rejects with an array
 * of `tasks` rejections
 * @param tasks tasks to complete
 * @param fallback the value to return if everything in `tasks` rejected
 */
export default async function pSomeFirst<T>(
	tasks: Iterable<CancelablePromise<T>>,
	fallback?: T | PromiseLike<T>
): Promise<T> {
	let ret: T | undefined = undefined;
	let rejections = [];

	for (const task of tasks) {
		try {
			ret = await task;
			break;
		} catch (e) {
			rejections.push(e);
		}
	} // Post Condition: `ret` is defined OR `tasks` have been fully consumed

	if (ret !== undefined) {
		// cancel remaining tasks
		for (const task of tasks) {
			task.catch((e) => {
				/* Suppress UnhandledPromiseRejectionWarnings when CanacelablePromises reject on cancel */
			});
			if (typeof task.cancel === "function") {
				task.cancel();
			} else {
				warnAboutNotCancelable();
			}
		}
		return ret;
	} else if (fallback !== undefined) {
		return fallback;
	} else {
		return Promise.reject(rejections);
	}
}

let haveWarnedAboutNotCancelable = false;
function warnAboutNotCancelable() {
	if (haveWarnedAboutNotCancelable) return;

	if (typeof process?.emitWarning === "function") {
		process.emitWarning(
			`Promises passed to p-some-first should be cancelable otherwise we must wait for all Promises to settle`
		);
	}
	haveWarnedAboutNotCancelable = true;
}

// shim for CommonJS
module.exports = pSomeFirst;
