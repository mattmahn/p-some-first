## [1.1.0](https://gitlab.com/mattmahn/p-some-first/compare/v1.0.2...v1.1.0) (2020-05-17)


### Features

* be more lax about accepted Promise types ([9c61735](https://gitlab.com/mattmahn/p-some-first/commit/9c61735e0b2865c4ce0cfa6ad877fa81fb45911d))

### [1.0.2](https://gitlab.com/mattmahn/p-some-first/compare/v1.0.1...v1.0.2) (2020-05-04)


### Bug Fixes

* include TypeScript declaration file ([faddfbe](https://gitlab.com/mattmahn/p-some-first/commit/faddfbe2e0c57f4d1ed0aba200b8c21902e1923c))

### [1.0.1](https://gitlab.com/mattmahn/p-some-first/compare/v1.0.0...v1.0.1) (2020-05-04)


### Bug Fixes

* include JavaScript in tarball ([41c048e](https://gitlab.com/mattmahn/p-some-first/commit/41c048e217a31930549317d993e2b1c0ed50bf1b))

## 1.0.0 (2020-05-04)


### Features

* initial implementation ([d4162c7](https://gitlab.com/mattmahn/p-some-first/commit/d4162c7db2e86edaa8817a1ec9869c0d4eec660a))
