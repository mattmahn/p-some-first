import assert from "assert";
import PCancelable from "p-cancelable";
import pSomeFirst from "p-some-first";

(async () => {
	assert.strictEqual(
		await pSomeFirst([
			new PCancelable((resolve, reject) => {
				setTimeout(() => resolve(1), 1e3);
			}),
			new PCancelable((resolve, reject, onCancel) => {
				const t = setTimeout(() => {
					reject(2);
				}, 100e3);
				onCancel(() => clearTimeout(t));
			}),
			new PCancelable((resolve) => resolve(2)),
		]),
		1
	);

	console.log("ES module ok");
})();
