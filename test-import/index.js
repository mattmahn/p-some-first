const assert = require("assert").strict;
const PCancelable = require("p-cancelable");
const pSomeFirst = require("p-some-first");

(async () => {
	assert.strictEqual(
		await pSomeFirst([
			new PCancelable((resolve, reject) => {
				setTimeout(() => resolve(1), 1e3);
			}),
			new PCancelable((resolve, reject, onCancel) => {
				const t = setTimeout(() => {
					reject(2);
				}, 100e3);
				onCancel(() => clearTimeout(t));
			}),
			new PCancelable((resolve) => resolve(2)),
		]),
		1
	);

	console.log("CommonJS module ok");
})();
