import PCancelable from "p-cancelable";

import pSomeFirst from "./";

test("accepts an array", async () => {
	await expect(
		pSomeFirst([new PCancelable((resolve) => resolve(3))])
	).resolves.toBe(3);
});

test("rejects with no rejections when given an empty array", async () => {
	await expect(pSomeFirst([])).rejects.toEqual([]);
});

test("rejects with all rejections when nothing resolves", async () => {
	const y = 42;
	await expect(
		pSomeFirst([
			new PCancelable((resolve, reject) => reject("error message")),
			new PCancelable((resolve, reject) =>
				reject(new Error("intentional"))
			),
			new PCancelable((resolve, reject) => reject(y)),
		])
	).rejects.toEqual(["error message", new Error("intentional"), y]);
});

test("resolves with fallback when given empty array", async () => {
	await expect(pSomeFirst([], 1)).resolves.toBe(1);
});

test("resolves with second value", async () => {
	await expect(
		pSomeFirst([
			new PCancelable((resolve, rejct) =>
				rejct(new Error("intentional"))
			),
			new PCancelable((resolve) => resolve(1)),
		])
	).resolves.toBe(1);
});

test("resolves with fallback when nothing resolves", async () => {
	await expect(
		pSomeFirst(
			[
				new PCancelable((resolve, reject) =>
					reject(new Error("intentional"))
				),
				new PCancelable((resolve, reject) =>
					reject(new Error("intentional"))
				),
				new PCancelable((resolve, reject) =>
					reject(new Error("intentional"))
				),
			],
			1
		)
	).resolves.toBe(1);
});

test("cancels remaining promises after first resolved", async () => {
	const cancelTimer = jest.fn((timer) => clearTimeout(timer));

	const actual = await pSomeFirst([
		new PCancelable((resolve, reject) => {
			setTimeout(() => resolve(1), 1e3);
		}),
		new PCancelable((resolve, reject, onCancel) => {
			const t = setTimeout(() => {
				reject(2);
			}, 100e3);
			onCancel(() => cancelTimer(t));
		}),
		new PCancelable((resolve) => resolve(2)),
	]);

	expect(actual).toBe(1);
	expect(cancelTimer).toHaveBeenCalledTimes(1);
});
