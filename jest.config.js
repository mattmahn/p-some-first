module.exports = {
	preset: "ts-jest",

	collectCoverage: true,
	coverageReporters: ["text", "html", "cobertura"],
	restoreMocks: true,
	testEnvironment: "node",
	testPathIgnorePatterns: ["dist/", "node_modules/"],
};
