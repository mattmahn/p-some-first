# p-some-first

Like `Promise.all`/`Promise.any` but only returns the first resolved value.

```js
import PCancelable from "p-cancelable";
import pSomeFirst from "p-some-first";

(async () => {
	await pSomeFirst([
		new PCancelable((resolve) => setTimeout(() => resolve(1), 1000)),
		new PCancelable((resolve) => resolve(2)),
	]);
	// resolves to 1

	await pSomeFirst([
		new PCancelable((resolve, reject) => reject("error message")),
		new PCancelable((resolve, reject) => reject(new Error("intentional"))),
		new PCancelable((resolve, reject) => reject(42)),
	]);
	// rejects with ["error message", new Error("intentional"), 42]

	await pSomeFirst(
		[
			new PCancelable((resolve, reject) => reject("error message")),
			new PCancelable((resolve, reject) =>
				reject(new Error("intentional"))
			),
			new PCancelable((resolve, reject) => reject(42)),
		],
		7
	);
	// resolves to the fallback value (7)
})();
```

See the [tests](./index.test.ts) for more examples.

Cancelable Promises SHOULD be used otherwise there is hardly any difference between using p-some-first and this:

```js
Promise.allSettled([
	Promise.reject(new Error("something")),
	Promise.resolve(42),
]).then((rr) => {
	return rr.find((r) => r.status === "fulfilled").value;
});
```

By using cancelable Promises, after one Promise resolves, all remaining Promises can be canceled and `pSomeFirst` will return without waiting for all promises to settle.
Cancelable Promises will also help to minimize any errant UnhandledPromiseRejectionWarnings.
Until JavaScript gets a way to cancel native Promises, you can use a library like [p-cancelable][] or [Bluebird](http://bluebirdjs.com/docs/api/cancellation.html) to create cancelable Promises.

If cancelable Promises are not used (in a Node.js environment), a [process warning][] will be emitted notifying you about this.

[p-cancelable]: https://github.com/sindresorhus/p-cancelable/
[process warning]: https://nodejs.org/dist/latest/docs/api/process.html#process_event_warning
